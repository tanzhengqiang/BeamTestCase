package com.test;

import org.apache.beam.runners.direct.DirectRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.values.PCollection;

/**
 * Created by tzq on 2019/5/16.
 */
public class Demo {
    public static void main(String[] args) {
        // Create the pipeline.
        PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
        options.setRunner(DirectRunner.class); //使用本地的runner (默认的方式)

        Pipeline p = Pipeline.create(options);

        PCollection<String> lines = p.apply(
                "ReadMyFile", TextIO.Read.from("test.txt"));

        String name = lines.getName();
        System.out.println(name);

    }
}
